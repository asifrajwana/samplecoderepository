//
//  Strings.swift
//  ArmTickets
//
//  Created by Ourangzaib khan on 10/3/15.
//  Copyright © 2015 Ourangzaib khan. All rights reserved.
//

import UIKit

extension String {
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
}