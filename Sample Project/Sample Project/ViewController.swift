//
//  ViewController.swift
//  Sample Project
//
//  Created by Ourangzaib khan on 2/9/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {
    
    
    // MARK: - Class  Variables
    @IBOutlet weak var tableView: UITableView!
    var tableInfo =  NSMutableArray()
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    // MARK: - Class override methods
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Web Info"
        self.setTableVeiwData();
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - setDataForTable
    
    func setTableVeiwData(){
        let sharedinstace = Service()
        
        sharedinstace.getInfo(({ (series : NSMutableArray?, error: ErrorType?) -> Void in
            if(series !=  nil){
                self.tableInfo = series!
                self.tableView.reloadData()
            }
        }))
    }
    
    func reloadtableview(){
        dispatch_async(dispatch_get_main_queue(),{
            self.tableView.reloadData()
        })
    }
    
    
    
    // MARK: - Table View DataSource
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableInfo.count > 0){
            return tableInfo.count;
        }
        return 0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! InfoTableViewCell
        
        let Dictionary = self.tableInfo[indexPath.row] as! NSDictionary
        print(Dictionary)
        var value = Dictionary["Date"]
        cell.dateLabel.text = String(value!)
        value = Dictionary["volume"]
        cell.volumeLabel.text = String(value!)
       value = Dictionary["open"]
        cell.openLabel.text = String(value!)
        return cell
        
    }
    

}

